package com.company;

import java.util.ArrayList;

public class SorteiaCarta {
    ArrayList<String> carta = new ArrayList<String>();
    ArrayList<Integer> valorCarta = new ArrayList<Integer>();

    public void setCarta(ArrayList<String> carta) {
        this.carta = carta;
    }

    public void setValorCarta(ArrayList<Integer> valorCarta) {
        this.valorCarta = valorCarta;
    }

    public void sorteio(){
        System.out.println(this.carta);
    }
}
