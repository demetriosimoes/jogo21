package com.company;

import java.util.ArrayList;
import java.util.Random;

public class Baralho {
    ArrayList<String> cartaNipe = new ArrayList<String>();
    ArrayList<String> cartaValor = new ArrayList<String>();
    ArrayList<String> baralho = new ArrayList<String>();
    String cartaSorteada;
    Integer valorCartaSorteada;

    //ArrayList<Integer> cartaValor = new ArrayList<Integer>();
    public Baralho() {
        this.cartaNipe.add("espada");
        this.cartaNipe.add("paus");
        this.cartaNipe.add("ouro");
        this.cartaNipe.add("copas");

        this.cartaValor.add("A");
        this.cartaValor.add("2");
        this.cartaValor.add("3");
        this.cartaValor.add("4");
        this.cartaValor.add("5");
        this.cartaValor.add("6");
        this.cartaValor.add("7");
        this.cartaValor.add("8");
        this.cartaValor.add("9");
        this.cartaValor.add("Q");
        this.cartaValor.add("J");
        this.cartaValor.add("k");

        for(int i=0;i<11;i++){
            for(int j=0;j<3;j++){
                this.baralho.add( this.cartaValor.get(i) + this.cartaNipe.get(j));
            }
        }
        //System.out.println("baralho:" + baralho);
    }
//    private String cartaSorteada;
//    private Integer cartaSorteadaValor;

    public void getCarta(){
        Random gerador = new Random();
        boolean bSorteio=false;
        while(!bSorteio){
            int i =gerador.nextInt(12*4);
            if(this.baralho.get(i)!=""){
                //System.out.println("carta sorteada:" + this.baralho.get(i));
                this.cartaSorteada=this.baralho.get(i);
                //valor carta
                this.valorCartaSorteada=i+1;

                this.baralho.set(i,"");
                bSorteio=true;
            }
        }
        System.out.println("baralho:" + this.baralho);
    }

}
